<?php

namespace Tests\Feature\Repositories;

use App\Models\Order as OrderModel;
use App\Repositories\Order as OrderRepository;
use Tests\TestCase;

class OrderTest extends TestCase
{

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->orderRepository = new OrderRepository(new OrderModel);
    }

    /**
     * @test
     */
    public function assignDriverCorrectly()
    {   
        $order = factory(OrderModel::class)->make();       

        $order = $this->orderRepository->assignDriver($order);

        $this->assertNotNull($order->id);
    }

}
