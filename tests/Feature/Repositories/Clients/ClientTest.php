<?php

namespace Tests\Feature\Repositories\Clients;

use App\Models\Clients\Client as ClientModel;
use App\Repositories\Clients\Client as ClientRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class ClientTest extends TestCase
{

    /**
     * @var ClientRepository
     */
    protected $clientRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->clientRepository = new ClientRepository(new ClientModel);
    }

    /**
     * Confirma que al insertar datos buenos devuelve una instancia suya.
     *
     * @test
     */
    public function saveGoodDataClientReturnHisInstance()
    {   
        $dataClient = factory(\App\Models\Clients\Client::class)->make()->toArray();

        $newClient = $this->clientRepository->save($dataClient);

        $this->assertInstanceOf(ClientModel::class, $newClient);
    }

    /**
     * Confirma que al insertar datos erroneos se arrojará una exceptión y devolverá null.
     *
     * @test
     */
    public function saveWrongDataClientReturnNull()
    {   
        $dataClient = factory(\App\Models\Clients\Client::class)->make([
            'email' => null,
        ])->toArray();

        $this->expectException(\Exception::class);

        $newClient = $this->clientRepository->save($dataClient);

        $this->assertNull($newClient);
    }

}
