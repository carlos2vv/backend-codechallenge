<?php

namespace Tests\Feature\Repositories\Clients;

use App\Models\Clients\Address as AddressModel;
use App\Repositories\Clients\Address as AddressRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AddressTest extends TestCase
{

    /**
     * @var AddressRepository
     */
    protected $addressRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->addressRepository = new AddressRepository(new AddressModel);
    }

    /**
     * Confirma que al insertar datos buenos devuelve una instancia suya.
     *
     * @test
     */
    public function saveGoodDataClientReturnHisInstance()
    {   
        $dataAddress = factory(\App\Models\Clients\Address::class)->make()->toArray();

        $newClient = $this->addressRepository->save($dataAddress);

        $this->assertInstanceOf(AddressModel::class, $newClient);
    }

    /**
     * Confirma que al insertar datos erroneos se arrojará una exceptión y devolverá null.
     *
     * @test
     */
    public function saveWrongDataClientReturnNull()
    {   
        $dataAddress = factory(\App\Models\Clients\Address::class)->make([
            'client_id' => null,
        ])->toArray();

        $this->expectException(\Exception::class);

        $newClient = $this->addressRepository->save($dataAddress);

        $this->assertNull($newClient);
    }

}
