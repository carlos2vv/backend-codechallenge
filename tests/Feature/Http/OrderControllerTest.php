<?php

namespace Tests\Feature\Http;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class OrderControllerTest extends TestCase
{

    /**
     * @test
     * @dataProvider dataOrderProvider
     */
    public function listDriverDate($dataOrder)
    {
        $headers = [
          'Accept' => 'application/json',
        ];

        $response = $this->post('/api/create-order', $dataOrder, $headers);

        $responseObject = json_decode($response->content());
        $driverId = $responseObject->order->driver_id;

        $response = $this->get(
          'api/get-orders-driver/' . $driverId . '/' . $dataOrder['order']['delivery_date'],
          $headers
        );

        $response->assertStatus(200);        
    }

    /**
     * @provider
     */
    public function dataOrderProvider()
    {

      $faker = \Faker\Factory::create();

      return [
        [
          [
            "client" => [
              "name" => $faker->firstName(),
              "surnames" => $faker->lastName . ' ' . $faker->lastName,
              "email" => $faker->email(),
              "phone" => $faker->phoneNumber,
            ],
            "address_delivery" => [
              "address" => $faker->streetAddress,
              "city" => $faker->city,
              "zip_code" =>$faker->postcode,
              "province" => $faker->state,
              "country_code" => "ES",
            ],
            "order" => [
              "delivery_date" => $faker->date('Y-m-d'),
              "start_time_interval" => "08:00",
              "end_time_interval" => "09:30",
            ],
          ],
        ]
      ];
    }



}
