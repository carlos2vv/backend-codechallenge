<?php

namespace Tests\Feature\Http;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateOrderTest extends TestCase
{

    /**
     * @test
     * @dataProvider dataOrderProvider
     */
    public function createOrderCorrectly($dataOrder)
    {
        $headers = [
          'Accept' => 'application/json',
        ];

        $response = $this->post('/api/create-order', $dataOrder, $headers);

        $this->assertDatabaseHas('clients', [
          'email' => $dataOrder['client']['email']
        ]);
        $this->assertDatabaseHas('addresses', [
          'address' => $dataOrder['address_delivery']['address']
        ]);
        $this->assertDatabaseHas('orders', [
          'delivery_date' => $dataOrder['order']['delivery_date']
        ]);

        $response->assertStatus(201);
    }

    /**
     * @provider
     */
    public function dataOrderProvider()
    {

      $faker = \Faker\Factory::create();

      return [
        [
          [
            "client" => [
              "name" => $faker->firstName(),
              "surnames" => $faker->lastName . ' ' . $faker->lastName,
              "email" => $faker->email(),
              "phone" => $faker->phoneNumber,
            ],
            "address_delivery" => [
              "address" => $faker->streetAddress,
              "city" => $faker->city,
              "zip_code" =>$faker->postcode,
              "province" => $faker->state,
              "country_code" => "ES",
            ],
            "order" => [
              "delivery_date" => $faker->date('Y-m-d'),
              "start_time_interval" => "08:00",
              "end_time_interval" => "09:30",
            ],
          ],
        ]
      ];
    }



}
