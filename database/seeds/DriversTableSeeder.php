<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DriversTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now('utc')->toDateTimeString();
    	$driversFake = ['GLS', 'Correos Express', 'DHL', 'UPS'];
    	$dataBatch = array_map(function($driver) use ($now) {
    		return [
    			'name'			=> $driver,
    			'created_at'	=> $now,
    			'updated_at'	=> $now,
    		];
    	}, $driversFake);

        DB::table('drivers')->insert($dataBatch);
    }
}
