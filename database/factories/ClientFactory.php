<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Clients\Client::class, function (Faker $faker) {
    return [
        'name'		=> $faker->firstName,
        'surnames'	=> $faker->lastName . ' ' . $faker->lastName,
        'email'		=> $faker->unique()->safeEmail,
        'phone'		=> $faker->phoneNumber,
    ];
});
