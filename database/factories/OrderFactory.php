<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Order::class, function (Faker $faker) {
    return [
        'client_id'     => function() {
            return factory(App\Models\Clients\Client::class)->create()->id;
        },
        'address_id'     => function() {
            return factory(App\Models\Clients\Address::class)->create()->id;
        },
        'driver_id'             => null,
        'delivery_date'         => $faker->date('Y-m-d'),
        'start_time_interval'   => '08:30',
        'end_time_interval'     => '10:30',        
    ];
});
