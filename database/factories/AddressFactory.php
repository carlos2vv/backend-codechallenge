<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Clients\Address::class, function (Faker $faker) {
    return [
        'address'		=> $faker->streetAddress,
        'city'			=> $faker->city,
        'zip_code'		=> $faker->postcode,
        'province'		=> $faker->state,
        'country_code'	=> 'ES', //$faker->countryCode
        'client_id'		=> function() {
        	return factory(App\Models\Clients\Client::class)->create()->id;
        },
    ];
});
