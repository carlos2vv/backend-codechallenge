<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    
    /**
     * Pedidos del driver
     */
    public function orders()
    {
    	return $this->hasMany('App\Models\Order');
    }
}
