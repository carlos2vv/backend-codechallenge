<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    ];

    /**
     * Cliente del pedido
     */
    public function client()
    {
    	return $this->belongsTo('App\Models\Clients\Client');
    }

    /**
     * Dirección de el pedido.
     * 
     * Ya que un cliente puede tener varias direcciones guardadas en la base
     * de datos, solo se asignará una
     */
    public function address()
    {
    	return $this->belongsTo('App\Models\Clients\Address');
    }


    /**
     * Driver del pedido
     */
    public function driver()
    {
    	return $this->belongsTo('App\Models\Driver');
    }

}
