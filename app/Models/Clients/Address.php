<?php

namespace App\Models\Clients;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    ];

    /**
     * Obtiene el cliente al que pertenece una dirección.
     * 
     * Podriamos definir una relación n:n, por ejemplo varios miembros
     * de la misma familia pueden compartir la misma dirección. Esto en
     * un principio añadiría la "complejidad" de tener que añadir la tabla
     * pivote y su correspondinte coste en rendimiento en máquinas al tener
     * consultas SQL algo más complejas.
     * En la práctica, conforme la aplicación crece, se podría hacer una
     * comprobación de cuantas direcciones hay repetidas y hacer un estudio
     * de si merece la pena hacer un n:n
     */
    public function client()
    {
    	return $this->belongsTo('App\Models\Clients\Client');
    }
}
