<?php

namespace App\Models\Clients;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    
	/**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    ];

    /**
     * Pedidos del cliente
     */
	public function orders()
	{
		return $this->hasMany('App\Models\Order');
	}


	/**
     * Un cliente puede tener varias direcciones guardadas,
	 * aunque para un pedido solo se asociará 1.
     */
	public function addresses()
	{
		return $this->hasMany('App\Models\Clients\Address');
	}

}
