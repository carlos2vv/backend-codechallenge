<?php

namespace App\Http\Requests\Orders;

use App\Rules\Drivers\CollectionTimeInterval as CollectionTimeIntervalRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "client.name"               => "required|min:4|max:20",
            "client.surnames"           => "required|min:4|max:40",
            "client.email"              => "bail|required|email|unique:clients,email",
            "client.phone"              => "required|min:9|max:20",
            "address_delivery.address"  => "required|min:5|max:70",
            "address_delivery.city"     => "required|min:3|max:50",

            /**
             * Cada país tiene un formato propio de códigos postales.
             * Aquí crearía una regla personalizada, la cual, dependiendo
             * de el país en el que se esté aplicará una regla u otra.
             */
            "address_delivery.zip_code"     => "required|min:3|max:20",
            "address_delivery.province"     => "required|min:3|max:30",
            "address_delivery.country_code" => "required|size:2",
            "order.delivery_date"           => "required|date_format:Y-m-d",
            "order.start_time_interval"     => "required|date_format:H:i",
            "order.end_time_interval"       => [
                "required",
                "date_format:H:i",
                new CollectionTimeIntervalRule(
                    $this->input('order.start_time_interval')
                ),
            ],
        ];
    }
}
