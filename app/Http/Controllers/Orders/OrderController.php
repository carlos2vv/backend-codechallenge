<?php

namespace App\Http\Controllers\Orders;

use App\Http\Requests\Orders\GetOrdersDriverRequest;
use App\Models\Order;
use App\Repositories\Order as OrderRepository;
use Illuminate\Http\Request;

class OrderController extends \App\Http\Controllers\Controller
{

    /**
     * Instancia de el repositorio order
     *
     * @var        App\Repositories\Order
     */
    protected $orderRepository;

    /**
     * { function_description }
     *
     * @param      OrderRepository  $orderRepository  The order repository
     */
    public function __construct(OrderRepository $orderRepository) {
        $this->orderRepository      = $orderRepository;
    }

    /**
     * Obtiene los pedidos de un driver para una fecha en concreto.
     */
    public function listDriverDate($idDriver, $date)
    {

        $this->orders = $this->orderRepository->getOrdersDriverDate($idDriver, $date);

        return response()->json([
                'orders' => $this->orders,
        ], 200);
        
    }

}
