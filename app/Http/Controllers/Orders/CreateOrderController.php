<?php

namespace App\Http\Controllers\Orders;

use App\Http\Requests\Orders\StoreOrderRequest;
use App\Models\Order;
use App\Repositories\Clients\Client as ClientRepository;
use App\Repositories\Clients\Address as AddressRepository;
use App\Repositories\Order as OrderRepository;
use Illuminate\Support\Facades\DB;

class CreateOrderController extends \App\Http\Controllers\Controller
{
    
    /**
     * Instancia de el repositorio cliente
     *
     * @var        App\Repositories\Clients\Client
     */
    protected $clientRepository;

    /**
     * Instancia de el repositorio address
     *
     * @var        App\Repositories\Clients\Address
     */
    protected $addressRepository;

    /**
     * Instancia de el repositorio order
     *
     * @var        App\Repositories\Order
     */
    protected $orderRepository;

    public function __construct(
        ClientRepository $clientRepository,
        AddressRepository $addressRepository,
        OrderRepository $orderRepository
    ) {
        $this->clientRepository     = $clientRepository;
        $this->addressRepository    = $addressRepository;
        $this->orderRepository      = $orderRepository;
    }

    /**
     * Guarda un nuevo pedido.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(StoreOrderRequest $request)
    {

        $this->setRequest($request);

        DB::beginTransaction();
        if (! $this->saveEntitiesOrder()) {
            DB::rollBack();
            return response()->json([
                'msg' => 'No se ha podido grabar el pedido',
            ], 204);
        }

        DB::commit();

        /**
         * Esto lo pondría en un evento
         */
        $this->order = $this->orderRepository->assignDriver($this->order);

        return response()->json([
                'order' => $this->order,
        ], 201);
        
    }

    /**
     * Establece la petición a nivel global de el controlador
     *
     * @param      Illuminate\Http\Request  $request  La petición
     */
    private function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * El post viene cargado con datos de diferentes entidades a guardar.
     */
    private function saveEntitiesOrder()
    {
        $dataClient = $this->request->only([
            'client',
        ]);

        if (! $client = $this->clientRepository->save($dataClient['client'])) {
            return false;
        }

        $dataAddress = $this->request->only([
            'address_delivery',
        ]);

        $dataAddress['address_delivery']['client_id'] = $client->id;

        if (! $address = $this->addressRepository->save($dataAddress['address_delivery'])) {
            return false;
        }

        $dataOrder = $this->request->only([
            'order',
        ]);

        $dataOrder['order']['client_id']    = $client->id;
        $dataOrder['order']['address_id']   = $address->id;

        if (! $order = $this->orderRepository->save($dataOrder['order'])) {
            return false;
        }

        $this->order = $order;

        return true;

    }
}
