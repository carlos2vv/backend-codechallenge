<?php

namespace App\Repositories;

use App\Models\Driver as DriverModel;
use Illuminate\Support\Facades\DB;

class Driver
{
	
	/**
	 * Instancia de el modelo Driver
	 */
	private $source;

	public function __construct(DriverModel $driverModel)
	{
		$this->source = $driverModel;
	}


	/**
     * Obtiene un driver aleatorio.
     *
     * @return DriverModel|null
     */
    public function getRandomDriver(): ?DriverModel
    {	
        return $this->source::inRandomOrder()->first();
    }

}