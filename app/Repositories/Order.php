<?php

namespace App\Repositories;

use App\Models\Order as OrderModel;
use App\Repositories\Driver as DriverRepository;

class Order
{
	
	/**
	 * Instancia de el modelo Order
	 */
	private $source;

    public function __construct(OrderModel $orderModel)
	{
        $this->source = $orderModel;
	}


	/**
     * Crea un nuevo pedido
     *
     * @param array $dataOrder
      * 
     * @return OrderModel|null
     */
    public function save(array $dataOrder): ?OrderModel
    {	
        $newOrder = $this->source::create($dataOrder);
        return $newOrder instanceof OrderModel ? $newOrder : null;
    }

    /**
     * Asigna un driver a un pedido.
     * Devuelve el pedido con el nuevo driver asociado o false en caso de error.
     *
     * @param      OrderModel  $order  El pedido
     * 
     * @return     App\Models\Order|false
     */
    public function assignDriver(OrderModel $order)
    {
        $driverRepository = new DriverRepository(new \App\Models\Driver);
        if (! $driver = $driverRepository->getRandomDriver()){
            return false;
        }
        
        $order->driver_id = $driver->id;
        $order->save();

        return $order;
    }

    /**
     * Obtiene los pedidos de un driver para un día en concreto
     *
     * @param      int      $idDriver
     * @param      string   $date
     *
     * @return     array    The orders driver date.
     */
    public function getOrdersDriverDate($idDriver, $date)
    {
        return $this->source::with(['client', 'address'])
            ->where('delivery_date', $date)
            ->get()
            ->toArray();
    }

}