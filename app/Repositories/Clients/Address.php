<?php

namespace App\Repositories\Clients;

use App\Models\Clients\Address as AddressModel;

class Address
{
	
	/**
	 * Instancia de el modelo
	 */
	private $source;

	public function __construct(AddressModel $addressModel)
	{
		$this->source = $addressModel;
	}


	/**
     * Crea una nueva dirección
     *
     * @param array $dataAddress
      * 
     * @return AddressModel|null
     */
    public function save(array $dataAddress): ?AddressModel
    {	
        $newAddress = $this->source::create($dataAddress);
        return $newAddress instanceof AddressModel ? $newAddress : null;
    }

}