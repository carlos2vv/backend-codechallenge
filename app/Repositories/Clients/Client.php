<?php

namespace App\Repositories\Clients;

use App\Models\Clients\Client as ClientModel;

class Client
{
	
	/**
	 * Instance resource
	 */
	private $source;

	public function __construct(ClientModel $client)
	{
		$this->source = $client;
	}


	/**
     * Crea un nuevo cliente
     *
     * @param array $dataClient
      * 
     * @return ClientModel|null
     */
    public function save(array $dataClient): ?ClientModel
    {	
        try {
            $newClient = $this->source::create($dataClient);
            return $newClient instanceof ClientModel ? $newClient : null;
        } catch (Exception $e) {
            return null;
        }            
    }

}