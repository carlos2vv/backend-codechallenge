<?php

namespace App\Rules\Drivers;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Carbon;

class CollectionTimeInterval implements Rule
{

    /**
     * La hora de comienzo de el intervalo de entrega
     *
     * @var        Illuminate\Support\Carbon
     */
    protected $startTimeInterval;

    /**
     * La hora de comienzo de el intervalo de entrega
     *
     * @var        Illuminate\Support\Carbon
     */
    protected $endTimeInterval;

    public function __construct(string $startTimeInterval)
    {
        $this->startTimeInterval = $this->getHourFromString($startTimeInterval);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (! $this->setEndHoursInterval($value)) {
            return false;
        }
        
        return $this->checkIntervalHours();
        
    }

    /**
     * Comprueba que el un rago de tiempo esté entre dos valores
     *
     * @return     bool
     */
    public function checkIntervalHours()
    {
        $minutesInterval = $this->startTimeInterval->diffInMinutes($this->endTimeInterval, false);
        return ((60 * 1) - 1) < $minutesInterval && $minutesInterval < ((60 * 8) + 1);
    }

    /**
     * Establece la hora final de un intervalo.
     *
     * @param      string   $endTimeInterval  Las hora en formato H:i
     *
     * @return     boolean
     */
    protected function setEndHoursInterval(string $endTimeInterval): bool
    {
        $this->endTimeInterval = $this->getHourFromString($endTimeInterval);

        return $this->startTimeInterval && $this->endTimeInterval;
    }

    /**
     * Obtiene la hora en una instacia de Carbon desde un formato string
     *
     * @param      string  $hour   La hora
     *
     * @return     static|CarbonInterface|false
     */
    private function getHourFromString($hour)
    {
        return Carbon::createFromFormat('H:i', $hour);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Se debe especificar una franja horaria entre 1 y 8 horas';
    }
}
