<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Rutas del api para tratar con la administración de los pedidos.
 */
Route::namespace('Orders')->group(function () {

	/**
	 * Genera un pedido.
	 */
	Route::post('create-order', 'CreateOrderController');

	/**
	 * Obtiene los pedidos para un driver y fecha
	 */
	Route::get('get-orders-driver/{id}/{date}', 'OrderController@listDriverDate');

});
